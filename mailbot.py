#coding: utf-8
#               ___                                                   ___     
#              /\__\                                                 /\__\    
#             /:/ _/_                     ___           ___         /:/ _/_   
#            /:/ /\__\                   /\__\         /\__\       /:/ /\__\  
#           /:/ /:/ _/_   ___     ___   /:/__/        /:/  /      /:/ /:/ _/_ 
#          /:/_/:/ /\__\ /\  \   /\__\ /::\  \       /:/__/      /:/_/:/ /\__\
#          \:\/:/ /:/  / \:\  \ /:/  / \/\:\  \__   /::\  \      \:\/:/ /:/  /
#           \::/_/:/  /   \:\  /:/  /   ~~\:\/\__\ /:/\:\  \      \::/_/:/  / 
#            \:\/:/  /     \:\/:/  /       \::/  / \/__\:\  \      \:\/:/  /  
#             \::/  /       \::/  /        /:/  /       \:\__\      \::/  /   
#              \/__/         \/__/         \/__/         \/__/       \/__/    
#
from selenium import webdriver
import time
import random
import threading
import os
import base64

print (base64.b64decode(r'''
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLyQkICAg
ICAgIC8kJCAvJCQgLyQkICAgICAgICAgICAgICAgICAgICANCiAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfCAkJCAgICAgIHxfXy98ICQkfCAkJCAgICAg
ICAgICAgICAgICAgICAgDQogIC8kJCQkJCQgICAvJCQkJCQkICAgLyQkJCQkJCAgIC8kJCQkJCQg
IC8kJCAgIC8kJHwgJCQgICAvJCQgLyQkfCAkJHwgJCQgIC8kJCQkJCQgICAvJCQkJCQkIA0KIC8k
JF9fICAkJCAvJCRfXyAgJCQgLyQkX18gICQkIC8kJF9fICAkJHwgICQkIC8kJC98ICQkICAvJCQv
fCAkJHwgJCR8ICQkIC8kJF9fICAkJCAvJCRfXyAgJCQNCnwgJCQgIFwgJCR8ICQkICBcX18vfCAk
JCAgXCAkJHwgJCQkJCQkJCQgXCAgJCQkJC8gfCAkJCQkJCQvIHwgJCR8ICQkfCAkJHwgJCQkJCQk
JCR8ICQkICBcX18vDQp8ICQkICB8ICQkfCAkJCAgICAgIHwgJCQgIHwgJCR8ICQkX19fX18vICA+
JCQgICQkIHwgJCRfICAkJCB8ICQkfCAkJHwgJCR8ICQkX19fX18vfCAkJCAgICAgIA0KfCAkJCQk
JCQkL3wgJCQgICAgICB8ICAkJCQkJCQvfCAgJCQkJCQkJCAvJCQvXCAgJCR8ICQkIFwgICQkfCAk
JHwgJCR8ICQkfCAgJCQkJCQkJHwgJCQgICAgICANCnwgJCRfX19fLyB8X18vICAgICAgIFxfX19f
X18vICBcX19fX19fXy98X18vICBcX18vfF9fLyAgXF9fL3xfXy98X18vfF9fLyBcX19fX19fXy98
X18vICAgICAgDQp8ICQkICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIA0KfCAkJCAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICANCnxfXy8gICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
ICAgICAgDQo=
'''))

SYMBOLS = 3
LETTERS = "abcdefghijklmnopqrstuvwxyz0123456789"
NUMBERS = "0123456789"
REG_URL = "https://service.mail.com/registration.html?edition=int&lang=en&#.1258-header-signup2-1"
PROXY_LIST = ["192.241.152.114:3128",
			  "144.217.12.240:8080",
			  "144.217.33.50:3128",
			  "181.215.109.123:8080",
			  "45.55.238.49:8080",
			  "45.55.72.162:8080",
			  "52.67.83.235:80",
			  "209.150.146.28:8080",
			  "162.243.32.182:80",
			  "4.31.35.110:8080",
			  "104.236.222.82:8080",
			  "162.243.32.182:3128",
			  "144.217.203.148:8080"]

def rand_name(royal=True):
	if royal:
		num=str(random.choice(NUMBERS))
		let = str(random.choice(LETTERS))
		return "{}{}{}".format(num,num,let)
	else:
		return "".join([ random.choice(list(LETTERS)) for j in range(SYMBOLS)])

def run_thread():
	print ("[OK] thread start ...")
	driver = webdriver.PhantomJS(os.path.join(os.getcwd(),"phantomjs.exe"),service_args=["--proxy=%s" %random.choice(PROXY_LIST)] )
	driver.get(REG_URL)

	while 1:
		try:
			
			account = rand_name()
			driver.find_element_by_xpath('//*[@id="id1e"]').send_keys(account)
			btn = driver.find_element_by_class_name("Request")
			btn.click()
			time.sleep(4)
			try:
				driver.find_element_by_class_name("feedbackPanelINFO")
				print ("good: %s"%account)
				with open('good.txt','a+') as f:
					f.write ("%s@mail.com\n"%account)
			except: print ("bad: %s" %account)
			try:
				driver.find_element_by_xpath('//*[@id="id1e"]').clear()
			except: pass
		except:
			print ("[X] error thread closed")
			driver.close()
			break

for j in range(10):
	t = threading.Thread(target=run_thread)
	t.start()

